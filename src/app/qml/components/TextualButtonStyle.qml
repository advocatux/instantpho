import QtQuick 2.4
import Ubuntu.Components 1.3

AbstractButton {
    id: button
    action: modelData
    width: layout.width + units.gu(4)
    height: parent.height
    Rectangle {
        color: UbuntuColors.slate
        opacity: 0.1
        anchors.fill: parent
        visible: button.pressed
    }
    Row {
        id: layout
        anchors.centerIn: parent
        spacing: units.gu(1)
        Icon {
            anchors.verticalCenter: parent.verticalCenter
            width: visible ? units.gu(2) : 0
            height: width
            name: action.iconName
            source: action.iconSource
            visible: (name != "") || (source != "")
            color: {
                if (button.enabled)
                    return text === i18n.tr("Pick") ? theme.palette.selected.backgroundText : theme.palette.normal.backgroundText

                return theme.palette.disabled.backgroundText
            }
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            text: action.text
            font.weight: text === i18n.tr("Pick") ? Font.Normal : Font.Light
            // Hide text from overflow button of ActionBar
            visible: text !== "More"
            width: visible ? paintedWidth : 0
            font.capitalization: Font.AllUppercase
            color: {
                if (button.enabled)
                    return text === i18n.tr("Pick") ? theme.palette.selected.backgroundText : theme.palette.normal.backgroundText

                return theme.palette.disabled.backgroundText
            }
        }
    }
}
