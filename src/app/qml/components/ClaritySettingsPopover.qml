/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

Popover {
    id: clarityPopover
    contentWidth: units.gu(36)
    contentHeight: units.gu(18)

    property var proc

    PageHeader {
        id: buttonsHeader
        anchors.top: parent.top
        width: parent.width

        Row {
            anchors.fill: parent

            AbstractButton {
                id: button1
                width: parent.width * 0.5
                height: parent.height
                onClicked: {
                    proc.clarity = 0.0
                    PopupUtils.close(clarityPopover)
                }

                Rectangle {
                    anchors.fill: parent
                    color: theme.palette.highlighted.background
                    visible: button1.pressed
                }

                Label {
                    anchors.centerIn: parent
                    text: i18n.tr("Reset")
                    font.capitalization: Font.AllUppercase
                }
            }

            AbstractButton {
                id: button
                width: parent.width * 0.5
                height: parent.height
                onClicked: PopupUtils.close(clarityPopover)

                Rectangle {
                    anchors.fill: parent
                    color: theme.palette.highlighted.background
                    visible: button.pressed
                }

                Label {
                    anchors.centerIn: parent
                    text: i18n.tr("OK")
                    font.capitalization: Font.AllUppercase
                }
            }
        }
    }

    Item {
        height: units.gu(12)
        anchors {
            top: buttonsHeader.bottom
            left: parent.left
            right: parent.right
        }

        Slider {
            anchors.centerIn: parent
            width: parent.width - units.gu(8)

            minimumValue: 0.0
            maximumValue: 1.0
            live: true

            function formatValue(v) {
                return (v * 100).toFixed(0)
            }

            value: proc.clarity
            onValueChanged: proc.clarity = value
            style: SliderStyle {}
        }
    }
}
