#include <QCoreApplication>
#include <QImage>
#include <QDebug>

#include <QtCore/qmath.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QImage img(256, 99, QImage::Format_RGB32);

    for (int x=0; x<256; ++x) {
        for (int y=0; y<99; ++y) {
            // Brightness
            qreal br = qPow(qreal(x) / 255, 1.5 - qreal(y) / 100.0) * 255.0;
            int brightness = qBound(0, int(br), 255);

            // Contrast
            qreal adj = qreal(y);
            adj *= 2;
            adj /= 100.0;
            adj *= 0.25;
            adj += 0.75;

            qreal tmp = qreal(x);
            tmp /= 255.0;
            tmp -= 0.5;
            tmp *= adj;
            tmp += 0.5;
            tmp *= 255.0;


            int contrast = qBound(0, int(tmp), 255);

            QRgb value = qRgb(brightness, contrast, 0);
            img.setPixel(x,y, value);
        }
    }

    qDebug() << "Map saved?" << img.save("/tmp/brightnessContrastMap.png", "PNG", 100);

    return a.exec();
}

