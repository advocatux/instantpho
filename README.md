InstantPho
==========

This is a fork of the former [InstantFX](https://launchpad.net/instantfx) app
made for the Ubuntu phone platform, by Stefano Verzegnassi.

InstantPho is a simple photo editor app, which provides for some basic
cropping, color enhancement, brightness and contrast control, and a set of
stylized filters. Images are imported from the originating app for editing,
and then exported back to the Gallery or other app for saving or uploading
to your preferred web site.
